//
//  iBonusViewModel.swift
//  BooksSharing
//
//  Created by Sergey Spevak on 08.03.2021.
//

import Foundation
import Combine



@available(iOS 13.0, *)
public class IPBBonusViewModel: ObservableObject
{
    
    @Published public var accessToken: String = ""
    
    @Published public var data: DataInfoByAvailableBonuses = DataInfoByAvailableBonuses()



    private var cancelableSet: Set<AnyCancellable> = []
    
    
    let host = IPBSettings.shared.hostBonus + "/api/v3/ibonus"
    
    public init()
    {
        $accessToken
            .flatMap{(accessToken)->AnyPublisher<DataInfoByAvailableBonuses, Never> in
                self.fetchGeneralInfo(accessToken: accessToken)
                    .map{$0}
                    .eraseToAnyPublisher()

            }
            .assign(to: \.data, on :self)
            .store(in: &self.cancelableSet)

    }
    
    
    
    func fetchGeneralInfo(accessToken: String)-> AnyPublisher<DataInfoByAvailableBonuses, Never> {

           guard let url = URL(string: host + "/generalinfo/" + accessToken)
               else {
            return Just(DataInfoByAvailableBonuses()).eraseToAnyPublisher()
           }

//        getGeneralInfo(accessToken: accessToken) { info in
//            //print(info.data?.currentQuantity)
//        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        print("fetchGeneralInfo")
        
        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        decoder.dateDecodingStrategy = .formatted(formatter)

            

            return URLSession.shared.dataTaskPublisher(for: request)
                .map{$0.data}
                .decode(type: InfoByAvailableBonuses.self, decoder: decoder)
                .map{$0.data!}
                .replaceError(with: DataInfoByAvailableBonuses())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
        }
    

    
    
    
    
    //Старый вариант
    func getGeneralInfo(accessToken: String, handler: @escaping (InfoByAvailableBonuses) -> Void)
    {
        let url = URL(string: host + "/generalinfo/" + accessToken)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
    
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .formatted(IPBSettings.shared.formatter)
                        
                        
                        let result = try decoder.decode(InfoByAvailableBonuses.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(InfoByAvailableBonuses(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(InfoByAvailableBonuses(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
    }
    
    
}
