//
//  File.swift
//  
//
//  Created by Sergey Spevak on 19.04.2021.
//

import Foundation
import Combine

public struct ParamsChargingBonuses: Codable
{
    public var Quantity: Decimal
    public var IDShop: String
    public var IDDoc: String

    public var IDBonusType: String
     
}

public class IPBBonusChargingShared
{
    let host = IPBSettings.shared.hostBonus + "/api/v3/ibonus"
    
    public static let shared = IPBBonusChargingShared()
    
    public init()
    {
        
    }
    
    public func charging(accessToken: String, idBonusType: String, sumBonuses: Decimal, idShop: String, idDoc: String,  handler: @escaping (ResultOperation)->Void)
    {
        let paramsBody = ParamsChargingBonuses(Quantity: sumBonuses, IDShop: idShop, IDDoc: idDoc, IDBonusType: idBonusType)
        
        let url = URL(string: host + "/charging/" + accessToken)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            
            let jsonData = try JSONEncoder().encode(paramsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultOperation.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultOperation.Network403())
                    }
                    
                }catch let jsonErr{
                    handler(ResultOperation.NetworkError())
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
        
        
    }
    
}
