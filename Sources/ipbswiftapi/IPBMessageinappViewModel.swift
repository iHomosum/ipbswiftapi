//
//  iBonusViewModel.swift
//  BooksSharing
//
//  Created by Sergey Spevak on 08.03.2021.
//
#if canImport(UIKit)

import Foundation
import Combine

public enum ChannelMessage {
    case InApp
    case InAppPromo
    case InAppService
    
}





@available(iOS 13.0, *)
public class IPBMessageinappViewModel: ObservableObject
{
    
    @Published public var accessToken: String = ""
    @Published public var channelMessage: ChannelMessage = ChannelMessage.InApp
    @Published public var dataList: [RGMessageForSend] = [RGMessageForSend]()

    //Нужен чтобы иметь доступ к host
    public static var shared: IPBMessageinappViewModel = IPBMessageinappViewModel()

    private var cancelableSet: Set<AnyCancellable> = []
    
    
    
    
    
    let host = IPBSettings.shared.hostMessagesinapp + "/api/v1/messageinapp"
    
    public init()
    {
        $accessToken
            .flatMap{(accessToken)->AnyPublisher<[RGMessageForSend], Never> in
                self.fetchListData(accessToken: accessToken, channelMessage: self.channelMessage)
                    .map{$0}
                    .eraseToAnyPublisher()

            }
            .assign(to: \.dataList, on :self)
            .store(in: &self.cancelableSet)

    }
    
    func fetchListData(accessToken: String, channelMessage: ChannelMessage)-> AnyPublisher<[RGMessageForSend], Never> {
        
        
        var Suffix = "inappmessages"
        
        switch channelMessage
        {
        case ChannelMessage.InApp: Suffix = "inappmessages"
            break
        case ChannelMessage.InAppPromo:  Suffix = "unreadinapppromomessages"
            break
        case ChannelMessage.InAppService:  Suffix = "unreadinappservicemessages"
            break
        }
        
        

        guard let url = URL(string: host + "/" + accessToken + "/" + Suffix)
               else {
            return Just([RGMessageForSend]()).eraseToAnyPublisher()
           }
        
        //print(url)



        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")


            return URLSession.shared.dataTaskPublisher(for: request)
                .map{$0.data}
                .decode(type: ResultInAppMessages.self, decoder: decoder)
                .map{($0.dataList ?? [])}
                .replaceError(with: [RGMessageForSend]())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
        }
    
    
    
    public func setMessagesAsRead(idMessages: String, handler: @escaping (ResultOperation) -> Void)
    {
        let url = URL(string: host + "/" + idMessages + "/setmessageasread")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
    
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .formatted(IPBSettings.shared.formatter)
                        
                        
                        let result = try decoder.decode(ResultOperation.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultOperation.Network403())
                    }
                    
                }catch let jsonErr{
                    handler (ResultOperation.NetworkError())
                    print(jsonErr)
               }

         
        }
        task.resume()
    }
  
    
    
}
#endif
