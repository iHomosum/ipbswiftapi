//
//  iProBonusGeneralSettings.swift
//  BooksSharing
//
//  Created by Sergey Spevak on 08.03.2021.
//

import Foundation
 
public class IPBSettings
{
    static public let shared = IPBSettings()
    
    public var defaultHost = "https://mp1.iprobonus.com"
    public var hostSCRM = "https://mp1.iprobonus.com"
    public var hostBonus = "https://mp1.iprobonus.com"
    public var hostMessagesinapp = "https://mp1.iprobonus.com"
    public var hostIEC = "http://84.201.188.117:6023"//"https://mp1.iprobonus.com"
    public var hostIMarketing = "https://mp1.iprobonus.com"
    
    public var accessKey: String = "f75f86d2-f6ab-4884-a475-b3b3f5469488"
    
    
    
    
    public let formatter = DateFormatter()
    
    init()
    {
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    }
    
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest
    {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    
    
}
