//
//  File.swift
//  
//
//  Created by Sergey Spevak on 18.10.2022.
//

import Foundation
//
//  MediaDataRemoveShared.swift
//  hassp
//
//  Created by Sergey Spevak on 18.10.2022.
//

import Foundation

public class MediaDataRemoveV7Shared
{
    public static var shared:MediaDataRemoveV7Shared = MediaDataRemoveV7Shared()
    
    public func fetchRemove(idRG: String, handler: @escaping (ResultEmpty)->Void)
    {
        
        if idRG == ""
        {
            handler(ResultEmpty(resultInit: ResultOperation.InitData()))
        }
        
        let url = URL(string: "http://84.201.188.117:5089/v7/mobile/" + idRG )
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "DELETE"
        // Set HTTP Request Header
        
        request.allHTTPHeaderFields = [
            "AccessKey": IPBSettings.shared.accessKey
        ]
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let error = error { print("Error took place \(error)"); return }
            guard let data = data else {return}
            
            if let jsonString = String(data: data, encoding: .utf8) {
                print(jsonString)
            }
            
            
            do {
                let httpResponse = response as! HTTPURLResponse
                print(httpResponse.statusCode)
                if httpResponse.statusCode == 200 { handler(try decoder.decode(ResultEmpty.self, from: data)) }
                if httpResponse.statusCode == 403 { handler(ResultEmpty(resultInit: ResultOperation.Network403())) }
                
            } catch let jsonErr { handler(ResultEmpty(resultInit: ResultOperation.NetworkError())); print(jsonErr)}
        }
        task.resume()
        
        
    }
}
