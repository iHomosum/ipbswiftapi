//
//  MediaDataListV7ViewModel.swift
//

import Foundation
import SwiftUI
import Combine

public class MediaDataListV7ViewModel : ObservableObject{
    
    @Published public var resultDataListMediaData: ResultDataListMediaData = ResultDataListMediaData(resultInit: ResultOperation.InitData())

    @Published public var idEntity:String = ""
    
    @Published public var update:Date = Date()
    
    private let host = "http://84.201.188.117:5089/v7/mobile"
    private var cancelableSet: Set<AnyCancellable> = []
    
    public init(idEntityInput: String){
        self.idEntity = idEntityInput
        $update
            .flatMap{(update)->AnyPublisher<ResultDataListMediaData, Never> in
                self.fetchMediaList(idEntities: self.idEntity)
            }
            .assign(to: \.resultDataListMediaData, on :self)
            .store(in: &self.cancelableSet)
    }
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    struct APIConstants {
        static let jsonDecoder: JSONDecoder = {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategyFormatters = [ DateFormatter.standardT,
                                                       DateFormatter.standard]
            return decoder
        }()
    }
    func fetch<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, Error> {
        URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data}
            .decode(type: T.self, decoder: APIConstants.jsonDecoder)
            //.mapError {error in print(error) as! any Error}
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    func fetchMediaList(idEntities: String)-> AnyPublisher<ResultDataListMediaData, Never>  {
        if idEntity == ""
        {
            return Just(ResultDataListMediaData(resultInit: ResultOperation.InitData())).eraseToAnyPublisher()
        }
        
        guard let url = URL(string: host + "/\(idEntities)/list") else {
            return Just(ResultDataListMediaData(resultInit: ResultOperation.InitData())).eraseToAnyPublisher()
           }
        let request = makeRequest(url: url, httpMethod: "GET")
        
        return fetch(request)
            .map {$0}
            .replaceError(with: ResultDataListMediaData(resultInit: ResultOperation.NetworkError()))
            .eraseToAnyPublisher()
    }
    
}


