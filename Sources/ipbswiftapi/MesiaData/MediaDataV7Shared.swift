//
//  File.swift
//  
//
//  Created by Sergey Spevak on 13.10.2022.
//

import Foundation
import SwiftUI

public class MediaDataV7Shared
{
    public static var shared: MediaDataV7Shared = MediaDataV7Shared()
    
    public func uploadImage(idEntity: String, typeContent: Int, entityTypeName: String, alias: String, tag: Int,  image: UIImage, handler: @escaping (ResultDataMediaData?)->Void) {
        
        guard let url = URL(string: "http://84.201.188.117:5089/v7/mobile/entity?idEntity=" + idEntity + "&typeContent=" + typeContent.description + "&entityTypeName=" + entityTypeName + "&alias=" + alias + "&tag=" + tag.description)
                
        else { handler(ResultDataMediaData(resultInit: ResultOperation.NetworkError())); return }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        let boundary = UUID().uuidString
        
        guard let mediaImage = Media(withImage: image, forKey: "file") else { return }
        let dataBody = createDataBody(media: [mediaImage], boundary: boundary)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        
        request.allHTTPHeaderFields = [
            "Content-Type": "multipart/form-data; boundary=\(boundary)",
            "AccessKey": IPBSettings.shared.accessKey
        ]
        
        request.httpBody = dataBody
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let error = error { print("Error took place \(error)"); return }
            guard let data = data else {return}
            
            
            do {
                let httpResponse = response as! HTTPURLResponse
                print(httpResponse.statusCode)
                if httpResponse.statusCode == 200 { handler(try decoder.decode(ResultDataMediaData.self, from: data)) }
                if httpResponse.statusCode == 403 { handler(ResultDataMediaData(resultInit: ResultOperation.Network403())) }
                
            } catch let jsonErr { handler(ResultDataMediaData(resultInit: ResultOperation.NetworkError())); print(jsonErr)}
        }
        task.resume()
    }
    
    
    func createDataBody(media: [Media]?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key)\"; filename=\"\(photo.fileName)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
    
}


struct Media {
    let key: String
    let fileName: String
    let data: Data
    let mimeType: String

    init?(withImage image: UIImage, forKey key: String) {
        self.key = key
        self.mimeType = "image/jpg"
        self.fileName = "\(UUID()).jpeg"

        guard let data = image.jpegData(compressionQuality: 1.0) else { return nil }
        self.data = data
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
