//
//  File.swift
//
//


import Foundation
import SwiftUI
import Combine

public class MediaDataV7ViewModel : ObservableObject{
    @Published public var dataMedia:ResultDataMediaData? = nil
    @Published public var idEntity:String = ""

    @Published public var update:Date = Date()
    
    private let host = "http://84.201.188.117:5089/v7/mobile"
    private var cancelableSet: Set<AnyCancellable> = []
    
    public init(){
        
        $update
            .flatMap{(update)->AnyPublisher<ResultDataMediaData?, Never> in
                self.fetchMedia(idEntity: self.idEntity)
            }
            .assign(to: \.dataMedia, on :self)
            .store(in: &self.cancelableSet)
        
    }
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    struct APIConstants {
        static let jsonDecoder: JSONDecoder = {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategyFormatters = [ DateFormatter.standardT,
                                                       DateFormatter.standard]
            return decoder
        }()
    }
    func fetch<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, Error> {
        URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data}
            .decode(type: T.self, decoder: APIConstants.jsonDecoder)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    
    func fetchMedia(idEntity: String)-> AnyPublisher<ResultDataMediaData?, Never>  {
        if idEntity == ""
        {
            return Just(nil).eraseToAnyPublisher()
        }
            
        guard let url = URL(string: host + idEntity) else {
            return Just(nil).eraseToAnyPublisher()
           }
        let request = makeRequest(url: url, httpMethod: "GET")

        return fetch(request)
            .map { $0}
            .replaceError(with: nil)
            .eraseToAnyPublisher()
    }
}


