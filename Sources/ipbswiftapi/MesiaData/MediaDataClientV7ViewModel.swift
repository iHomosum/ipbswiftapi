//
//  File.swift
//  
//
//  Created by Sergey Spevak on 13.10.2022.
//

import Foundation
import SwiftUI
import Combine

//Возвращает медиаданные привязанные к конкретному клиенту
public class MediaDataClientV7ViewModel : ObservableObject{
    @Published public var dataListMedia: [DataMedia] = []
    @Published public var accessToken:String = ""

    
    private let host = "http://84.201.188.117:5089/v7/mobile"
    private var cancelableSet: Set<AnyCancellable> = []
    
    public init(){
        
        $accessToken
            .flatMap{(accessToken)->AnyPublisher<[DataMedia], Never> in
                self.fetchMediaList(accessToken: accessToken)
            }
            .assign(to: \.dataListMedia, on :self)
            .store(in: &self.cancelableSet)
    }
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    struct APIConstants {
        static let jsonDecoder: JSONDecoder = {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategyFormatters = [ DateFormatter.standardT,
                                                       DateFormatter.standard]
            return decoder
        }()
    }
    func fetch<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, Error> {
        URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data}
            .decode(type: T.self, decoder: APIConstants.jsonDecoder)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    func fetchMediaList(accessToken: String)-> AnyPublisher<[DataMedia], Never>  {
        
        guard let url = URL(string: host + "/client/list") else {
            return Just([DataMedia]()).eraseToAnyPublisher()
           }
        var request = makeRequest(url: url, httpMethod: "GET")
        request.setValue(accessToken, forHTTPHeaderField: "AccessToken")
        return fetch(request)
            .map { (response: MediaResult) -> [DataMedia] in response.dataList!}
            .replaceError(with: [DataMedia]())
            .eraseToAnyPublisher()
    }
    
}


