//
//  File.swift
//  
//


import Foundation
import SwiftUI
import Combine

public class MediaDataViewModel : ObservableObject{
    @Published public var dataListMedia: [DataMedia] = []
    @Published public var dataMedia:DataMedia? = nil
    @Published public var idEntity:String = ""
    @Published public var idEntities:String = ""
    
    private let host = "http://84.201.188.117:5089/mobile"
    private var cancelableSet: Set<AnyCancellable> = []
    
    public init(){
        $idEntity
            .flatMap{(idEntity)->AnyPublisher<DataMedia?, Never> in
                self.fetchMedia(idEntity: idEntity)
            }
            .assign(to: \.dataMedia, on :self)
            .store(in: &self.cancelableSet)
        $idEntities
            .flatMap{(idEntities)->AnyPublisher<[DataMedia], Never> in
                self.fetchMediaList(idEntities: idEntities)
            }
            .assign(to: \.dataListMedia, on :self)
            .store(in: &self.cancelableSet)
    }
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    struct APIConstants {
        static let jsonDecoder: JSONDecoder = {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategyFormatters = [ DateFormatter.standardT,
                                                       DateFormatter.standard]
            return decoder
        }()
    }
    func fetch<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, Error> {
        URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data}
            .decode(type: T.self, decoder: APIConstants.jsonDecoder)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    func fetchMediaList(idEntities: String)-> AnyPublisher<[DataMedia], Never>  {
        guard let url = URL(string: host + "/\(idEntities)/list") else {
            return Just([DataMedia]()).eraseToAnyPublisher()
           }
        let request = makeRequest(url: url, httpMethod: "GET")
        
        return fetch(request)
            .map { (response: MediaResult) -> [DataMedia] in response.dataList!}
            .replaceError(with: [DataMedia]())
            .eraseToAnyPublisher()
    }
    func fetchMedia(idEntity: String)-> AnyPublisher<DataMedia?, Never>  {
        guard let url = URL(string: host + idEntity) else {
            return Just(nil).eraseToAnyPublisher()
           }
        let request = makeRequest(url: url, httpMethod: "GET")

        return fetch(request)
            .map { (response: MediaResult) -> DataMedia in response.data!}
            .replaceError(with: nil)
            .eraseToAnyPublisher()
    }
}

public struct MediaResult:Codable {
    public var result: ResultOperation
    public var dataList: [DataMedia]?
    public var data:DataMedia?
}
