import SwiftUI
import Foundation

public class IPBGeneralOperation
{
    public init()
    {
        
    }
    
    public static var shared: IPBGeneralOperation = IPBGeneralOperation()
    
    
    public func getListURLImages(imageDataJSON: String?, urlImageDefault: String = "https://via.placeholder.com/500")->[URL]
    {
        var listURL = [URL]()
        
        if  let data = imageDataJSON!.data(using: .utf8)
        {
            do{
                let decoder = JSONDecoder()

                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"

                decoder.dateDecodingStrategy = .formatted(formatter)
                decoder.keyDecodingStrategy = .convertFromSnakeCase

                let todoItemModel = try decoder.decode(ListImagesAgile.self, from: data)
                print("Response data:\n \(todoItemModel)")
                
                for itemImageURLString in todoItemModel.datalist!
                {
                    listURL.append(URL(string: itemImageURLString.url!)!)
                }
               
            } catch _{
                listURL.append(URL(string: urlImageDefault)!)
                return listURL
           }
        }
        if listURL.count == 0
        {
            listURL.append(URL(string: urlImageDefault)!)
        }
        return listURL
    }
}
