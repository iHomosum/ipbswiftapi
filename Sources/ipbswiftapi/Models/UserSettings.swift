//
//  UserSettings.swift
//  April
//
//  Created by Sergey Spevak on 20.12.2020.
//

import Foundation
import Combine

@available(iOS 13.0, *)
public class UserSettings: ObservableObject {
    
    public init()
    {
        userName = UserDefaults.standard.object(forKey: "userName") as? String ?? ""
        userID = UserDefaults.standard.object(forKey: "userID") as? String ?? ""
        deviceID = UserDefaults.standard.object(forKey: "deviceID") as? String ?? ""
        
        language = UserDefaults.standard.object(forKey: "language") as? String ?? ""
        
        mentorID = UserDefaults.standard.object(forKey: "mentorID") as? String ?? ""
        
        phoneReg = UserDefaults.standard.object(forKey: "phoneReg") as? String ?? ""
        
        score = UserDefaults.standard.object(forKey: "score") as? Int ?? 0
        
        debugMode = UserDefaults.standard.object(forKey: "debugMode") as? Bool ?? false
        
        firstStart = UserDefaults.standard.object(forKey: "firstStart") as? String ?? ""
        
        let stringInternalClient = UserDefaults.standard.object(forKey: "internalClient") as? String ?? ""
        
        
        if stringInternalClient != ""
        {
            
            let jsonDecoder = JSONDecoder()
            let jsonData = stringInternalClient.data(using: .utf8)!
            do
            {
                self.internalClient = try jsonDecoder.decode(InternalClient.self, from: jsonData)
            }
                catch
                    {
                        self.internalClient = nil
                        print("error convert to internal client")
                    }
               
                
            
        }
        
        
    }
    
    @Published public var firstStart: String
    {
        didSet
        {
            UserDefaults.standard.set(firstStart, forKey: "firstStart")
        }
    }
    
    @Published public var debugMode: Bool
    {
        didSet
        {
            UserDefaults.standard.set(debugMode, forKey: "debugMode")
        }
    }
    
    @Published public var score: Int
    {
        didSet
        {
            UserDefaults.standard.set(score, forKey: "score")
        }
    }
    
    @Published public var userName: String {
        didSet
        {
            UserDefaults.standard.set(userName, forKey: "userName")
        }
    }
    
    @Published public var userID: String {
        didSet
        {
            UserDefaults.standard.set(userID, forKey: "userID")
        }
    }
    
    @Published public var deviceID: String
    {
        didSet
        {
            UserDefaults.standard.set(deviceID, forKey: "deviceID")
        }
    }
    
    @Published public var phoneReg: String
    {
        didSet
        {
            UserDefaults.standard.set(phoneReg, forKey: "phoneReg")
        }
    }
    
    
    
    @Published public var language: String
    {
        didSet
        {
            UserDefaults.standard.set(language, forKey: "language")
        }
    }
    
    
    @Published public var mentorID: String
    {
        didSet
        {
            UserDefaults.standard.set(mentorID, forKey: "mentorID")
        }
    }
    
    
    @Published public var internalClient: InternalClient?
    {
        didSet
        {
            
            UserDefaults.standard.set(internalClient.convertToString, forKey: "internalClient")
            
        }
    }
    
    
    
}

extension Encodable {
    var convertToString: String? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}
