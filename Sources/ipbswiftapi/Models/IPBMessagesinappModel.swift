//
//  File.swift
//  
//
//  Created by Sergey Spevak on 24.03.2021.
//

import Foundation

struct ResultInAppMessages: Codable {
    var result: ResultOperation?
    var dataList: [RGMessageForSend]?

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case dataList = "dataList"
    }
}

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

// MARK: - DataList
public struct RGMessageForSend: Codable, Hashable {
    public var idUnique: String?
        var idEnterprise: String?
        var idClient: String?
        var idMarketingEvent: String?
        var deliveryData: String?
        public var channel: Int?
        public var titleMessage: String?
        public var message: String?
        public var messageSecond: String?
        var statusSended: String?
        public var dateRead: String?
        var maxDateActual: String?
        var dateLoad: String?
        var dateToSend: String?
        var dateSent: String?
        var mediaDataLink: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case idEnterprise = "idEnterprise"
        case idClient = "idClient"
        case idMarketingEvent = "idMarketingEvent"
        case deliveryData = "deliveryData"
        case channel = "channel"
        case titleMessage = "description"
        case message = "message"
        case messageSecond = "messageSecond"
        case statusSended = "statusSended"
        case dateRead = "dateRead"
        case maxDateActual = "maxDateActual"
        case dateLoad = "dateLoad"
        case dateToSend = "dateToSend"
        case dateSent = "dateSent"
        case mediaDataLink = "mediaDataLink"
    }
}
