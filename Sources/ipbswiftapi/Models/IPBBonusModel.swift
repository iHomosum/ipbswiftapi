//
//  iBonusModel.swift
//  BooksSharing
//
//  Created by Sergey Spevak on 08.03.2021.
//

import Foundation

public struct InfoByAvailableBonuses: Codable {
    public var resultOperation: ResultOperation?
    public var data: DataInfoByAvailableBonuses?

    public init(result: ResultOperation)
    {
        self.resultOperation = result
    }
}



public struct DataInfoByAvailableBonuses: Codable {
    public var typeBonusName: String?
    public var currentQuantity: Decimal?
    public var forBurningQuantity: Decimal?
    public var dateBurning: Date?
    

}
