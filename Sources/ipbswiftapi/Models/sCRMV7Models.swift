//
//  File.swift
//  
//
//  Created by Sergey Spevak on 04.01.2023.
//

import Foundation


struct IncomeChannelData: Codable {
    var ChannelType: Int?
    var ChannelData: String?
    
    public init(channelType: Int, channelData: String)
    {
        self.ChannelType = channelType
        self.ChannelData = channelData
    }

    enum CodingKeys: String, CodingKey {
        case ChannelType = "channelType"
        case ChannelData = "channelData"
    }
}



struct IncomeDataForEndLogin: Codable {
    var ChannelType: Int?
    var ChannelData: String?
    var CodeConfirm: String?
    var InfoDevice: String?
    
    public init(channelType: Int, channelData: String, codeConfirm: String, infoDevice: String)
    {
        self.ChannelType = channelType
        self.ChannelData = channelData
        self.CodeConfirm = codeConfirm
        self.InfoDevice = infoDevice
    }

    enum CodingKeys: String, CodingKey {
        case ChannelType = "channelType"
        case ChannelData = "channelData"
        case CodeConfirm = "codeConfirm"
        case InfoDevice = "infoDevice"
    }
}





// MARK: - DeviceData - Идентификатор устройства, который является RefreshToken-ом
public struct DeviceData: Codable {
    public var idDevice: String?
    
    public init(){}
    
    enum CodingKeys: String, CodingKey {
        case idDevice = "idDevice"
    }
}


// MARK: - IncomeDataCreateAccessToken - данные для получения AccessToken
public struct IncomeDataCreateAccessToken: Codable {
    var IDDevice: String?
    var Latitude: Int?
    var Longitude: Int?

    public init(idDevice: String, latitude: Int = 0, longitude: Int = 0)
    {
        self.IDDevice = idDevice
        self.Latitude = latitude
        self.Longitude = longitude
    }
    enum CodingKeys: String, CodingKey {
        case IDDevice = "idDevice"
        case Latitude = "latitude"
        case Longitude = "longitude"
    }
}
