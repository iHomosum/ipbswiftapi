import Foundation

public class ClientLocation
{
    public static let main = ClientLocation()
    
    public var latitude: Double
    public var longitude: Double
    
    public init ()
    {
        latitude = 0
        longitude = 0
    }
    
    
}

public struct ClientParamJSONWhithGeo: Codable
 {
    //Из ClientParamJSON
    public var idClient: String
    public var accessToken: String
    public var paramName: String
    public var paramValue: String
    
    
    
    public var latitude: Double = 0.0
    public var longitude: Double = 0.0

    public var sourceQuery: Int = 0

    
 }

public class verificationPhoneBoby: Codable
{
    public init (channelValue: String, confirmCode: String, channelName: String = "phone")
    {
        self.ChannelValue = channelValue
        self.ConfirmCode = confirmCode
        self.ChannelName = channelName
    }
    public var TokenIncome: String = ""
    public var ChannelName: String
    public var ChannelValue: String
    public var ConfirmCode: String
}

public class ResultVerificationChannelParametersV3: Codable
{
    public var result: ResultOperation?
    public var tokenIncome: String = ""
            
    //Внутренний токен, который в дальнейшем будет верифицировать операции
    //Если он валидный, то операцию можно проводить
    public var accessToken: String = ""
    
    public init(result: ResultOperation)
    {
        self.result = result
    }
}

//---------Модели данных после верификации канала


public struct ClientParamJSON:Codable
{
    public var idClient: String
    public var accessToken: String
    public var paramName: String
    public var paramValue: String
}

public class ResultAddDeviceV3: Codable
{
    public var result: ResultOperation?

    public var idDevice: UUID?
    
    public init(result: ResultOperation)
    {
        self.result = result
    }
}


public struct ResultClientQueryV3: Codable
    {
        

    //Для чего не совсем понятно
    public var status: Int

    public var result: ResultOperation
        

    public var client: Clients?

    public var clientAdditionalInfo: ClietnsAddtionalInfo?
    
    public init(result: ResultOperation)
    {
        self.result = result
        status = 0
        
    }
}


///Используетя для сохранения внутренних данных клиента
public struct InternalClient: Codable
{
    public init()
    {
        
    }
    public var name: String = ""
    public var soname: String = ""
    public var patronymic: String?
    public var dateOfBirth: Date = Date()
    public var dateOfRegister: Date?
    public var sex: Int = 0

}


public struct Clients: Codable
    {
    public var idUnique: UUID

    public var idEnterprise: UUID
    

    public var name: String?
    public var soname: String?
    public var patronymic: String?

    public var sex: Int?

    var dateOfBirth: Date?
    //var dateOfRegister: Date

    public var comment: String?
}

public struct ClietnsAddtionalInfo: Codable
{
    public var additionalInfo: String

    //Статус полноты информации. Если 0 - то все ОК, если нет, то смотреть AdditionalInfo
    public var statusCompletion: Int

    public var emailGeneral: String

    public var phoneGeneral: String
    
}


public struct ResultAuthV3: Codable
{

    public var result: ResultOperation
    public var accessToken: String = ""
    
    public init(result: ResultOperation)
    {
        self.result = result
    }
    
}
