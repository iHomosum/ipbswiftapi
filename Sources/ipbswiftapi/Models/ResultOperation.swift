import Foundation
import Combine

public  struct ResultOperation: Codable, Equatable
{
    /// <summary>
    /// Статус результата
    /// </summary>
    public var status:Int

    /// <summary>
    /// Сообщение, которые выдается пользователю
    /// </summary>
    public var  message: String

    /// <summary>
    /// Внутренее сообщение для разработчиков
    /// </summary>
    public var messageDev: String?
    
    /// <summary>
    /// Код  результата. Зарезервированное поле
    /// </summary>
    public var codeResult: Int

    /// <summary>
    /// Продолжительность операции в миллисекундах
    /// </summary>
    public var duration: Float

    /// <summary>
    /// Идентификатор лог файле, если есть запись об операции
    /// </summary>
    public var idLog: UUID
    
    public var xRequestID: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case messageDev = "messageDev"
        case codeResult = "codeResult"
        case duration = "duration"
        case idLog = "idLog"
        case xRequestID = "x-request-id"
    }
    
    public static func InitData()->ResultOperation
    {
        return ResultOperation(status: 100, message: "", messageDev: "", codeResult: 0, duration: 0, idLog: UUID())
    }
    
    public static func NetworkError()->ResultOperation
    {
        return ResultOperation(status: 4, message: "Ошибка сети", messageDev: "", codeResult: 0, duration: Float.random(in: 1..<100), idLog: UUID())
    }
    
    public  static func Network403()->ResultOperation
    {
        return ResultOperation(status: 4, message: "Ошибка сети отсутствует доступ", messageDev: "Status code 403", codeResult: 0, duration: Float.random(in: 1..<100), idLog: UUID())
    }
    
    public static func == (lhs: ResultOperation, rhs: ResultOperation) -> Bool {
            lhs.duration == rhs.duration//Вероятность одинаковой продолжительности мала
            //Но лучше в будущем сделать на основе x-request-id
            //idLog использовать нельзя, т.к. он возникает только в момент ошибок и является устаревшим
        }
    
}

//Тип для пустого результата, который содержит только статус
public struct ResultEmpty: Codable {
    
    init(resultInit: ResultOperation?)
    {
        result = resultInit
    }
    
    public var result: ResultOperation?
    public var totalNumberRecords: Int?

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case totalNumberRecords = "totalNumberRecords"
    }
}



public struct ResultData<T: Codable>: Codable
{
    public var result: ResultOperation?
    public var data: T?
    public var totalNumberRecords: Int?
    
    public init(result: ResultOperation)
    {
        self.result = result
    }
    
    enum CodingKeys: String, CodingKey {
        case result = "result"
        case data = "data"
        case totalNumberRecords = "totalNumberRecords"
    }
}

// Пустой класс предназначенный для формирования единого ответа ResultData, в случае если нужно вернуть только ResultOperation
public struct EmptyResultOperation: Codable
{
    public init(){}
}
