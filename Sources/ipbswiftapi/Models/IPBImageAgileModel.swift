//
//  File.swift
//  
//
//  Created by Sergey Spevak on 22.03.2021.
//

import Foundation


public struct ListImagesAgile: Codable {
    public var datalist: [ImageAgile]?
    var typeName: String?

    enum CodingKeys: String, CodingKey {
        case datalist = "datalist"
        case typeName = "TypeName"
    }
    
}

// MARK: - Datalist
public struct ImageAgile: Codable {
    public var url: String?
    public var fileName: String?
    public var order: Int?
    public var typeName: String?

    enum CodingKeys: String, CodingKey {
        case url = "URL"
        case fileName = "FileName"
        case order = "Order"
        case typeName = "TypeName"
    }
}
