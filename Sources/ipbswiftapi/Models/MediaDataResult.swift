//
//  File.swift
//  
//
//  Created by Sergey Spevak on 13.10.2022.
//

import Foundation



public struct ResultDataMediaData:Codable {
    public init(){}
    init(resultInit: ResultOperation?)
    {
        result = resultInit
        data = nil
    }
    public var result: ResultOperation?
    public var data:DataMedia?
}

public struct ResultDataListMediaData:Codable {
    public init(){}
    init(resultInit: ResultOperation?)
    {
        result = resultInit
        dataList = []
    }
    public var result: ResultOperation?
    public var dataList: [DataMedia]?
    
    
}





public struct DataMedia: Codable, Identifiable {
    public init(){}
    public var id: UUID = UUID()
    public var dateEvent: String?
    public var urlData: String?
    public var alias: String?
    public var contentType: Int?
    public var order: Int?
    public var stringData: String?
    public var dataJSON: String?
    public var previewText: String?
    public var tag: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "idUnique"
        case dateEvent
        case urlData
        case alias
        case contentType
        case order
        case stringData
        case dataJSON
        case previewText
        case tag
    }
}

public struct ImageUploadData: Codable, Hashable {
    public init(){}
    public var idUnique: String?
    public var dateEvent: String?
    public var urlData: String?
    public var alias: String?
    public var contentType: Int?
    public var order: Int?
    public var tag: Int?
    public var stringData: String?
    public var dataJSON: String?
    public var previewText: String?
    public var size: Int?
}
