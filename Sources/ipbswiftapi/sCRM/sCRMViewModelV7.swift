//
//  File.swift
//  
//
//  Created by Sergey Spevak on 04.01.2023.
//

import Foundation
import Combine

public class sCRMViewModelV7
{
    public static let shared = sCRMViewModelV7()
    
    
    let host = IPBSettings.shared.hostSCRM + "/api/v7/clients"
    
    
    public init()
    {
        
    }
    
    public func loginStartByPhone(phoneNumber: String, handler: @escaping (ResultData<EmptyResultOperation>) -> Void)
    {
        let url = URL(string: host + "/login/start")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            let ParamsBody = IncomeChannelData(channelType: 0, channelData: phoneNumber)
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)
            
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }
                
                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultData<EmptyResultOperation>.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultData(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultData(result: ResultOperation.NetworkError()))
                    print(jsonErr)
                }
                
                
            }
            task.resume()
        }
        catch
        {
            print("Error")
        }
        
    }
    
    
    public func loginEndByPhone(phoneNumber: String, codeConfirm: String, handler: @escaping (ResultData<DeviceData>) -> Void)
    {
        let url = URL(string: host + "/login/end")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            let ParamsBody = IncomeDataForEndLogin(channelType: 0, channelData: phoneNumber, codeConfirm: codeConfirm, infoDevice: "")
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)
            
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }
                
                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultData<DeviceData>.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultData(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultData(result: ResultOperation.NetworkError()))
                    print(jsonErr)
                }
                
                
            }
            task.resume()
        }
        catch
        {
            print("Error")
        }
        
    }
    
    public func getAccessToken(latitude: Int = 0, longitude: Int = 0, handler: @escaping (ResultData<String>) -> Void)
    {
        let url = URL(string: host + "/accesstoken")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            let ParamsBody = IncomeDataCreateAccessToken(idDevice: UserSettings().deviceID, latitude: latitude, longitude: longitude)
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)
            
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }
                
                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultData<String>.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultData(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultData(result: ResultOperation.NetworkError()))
                    print(jsonErr)
                }
                
                
            }
            task.resume()
        }
        catch
        {
            print("Error")
        }
        
    }
    
}
