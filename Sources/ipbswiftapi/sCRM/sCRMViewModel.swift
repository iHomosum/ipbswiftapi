//
//  sCRM.swift
//  April
//
//  Created by Sergey Spevak on 19.12.2020.
//

import Foundation
import Combine




@available(iOS 13.0, *)
public class sCRMViewModel
{
    public static let shared = sCRMViewModel()
    
    
    let host = IPBSettings.shared.hostSCRM + "/api/v3/clients"
    
    
    public init()
    {
        
    }
    
    public func verificationPhoneBegin(phoneNumber: String, channelName: String, handler: @escaping (ResultOperation) -> Void)
    {
        let url = URL(string: host + "/verificationchannelbegin")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            let ParamsBody = verificationPhoneBoby(channelValue: phoneNumber, confirmCode: "0", channelName: channelName)
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultOperation.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultOperation.Network403())
                    }
                    
                }catch let jsonErr{
                    handler(ResultOperation.NetworkError())
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }

    }
    
    
    public func verificationPhoneEnd(phoneNumber: String, codeConfirm: String, channelName: String, handler: @escaping (ResultVerificationChannelParametersV3) -> Void)
    {
        let url = URL(string: host + "/verificationchannelend")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            let ParamsBody = verificationPhoneBoby(channelValue: phoneNumber, confirmCode: codeConfirm, channelName: channelName)
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultVerificationChannelParametersV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultVerificationChannelParametersV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultVerificationChannelParametersV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }

    }
    
    public func createAT(handler: @escaping (ResultVerificationChannelParametersV3) -> Void)
    {
        let url = URL(string: host + "/createat")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        do
        {
            let ParamsBody = verificationPhoneBoby(channelValue: "", confirmCode: "", channelName: "device")
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultVerificationChannelParametersV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultVerificationChannelParametersV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultVerificationChannelParametersV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }

    }
    
    
    
    //---Регистрация нового клиента
    
    public func addClient(accessToken: String, handler: @escaping (ResultClientQueryV3) -> Void)
    {
        let url = URL(string: host)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            let ParamsBody = ClientParamJSON(idClient: "00000000-0000-0000-0000-000000000000", accessToken: accessToken, paramName: "createnewclient", paramValue: "")
            
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultClientQueryV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultClientQueryV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultClientQueryV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }
    
    
    public func addChannel(accessToken: String, idClient: String, channelData: String, paramName: String, handler: @escaping (ResultClientQueryV3) -> Void)
    {
        let url = URL(string: host + "/channel")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            let ParamsBody = ClientParamJSON(idClient: idClient, accessToken: accessToken, paramName: paramName, paramValue: channelData)
            
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultClientQueryV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultClientQueryV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultClientQueryV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }

    
    public func addPhone(accessToken: String, idClient: String, phone: String, paramName: String, handler: @escaping (ResultClientQueryV3) -> Void)
    {
        let url = URL(string: host + "/phone")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            let ParamsBody = ClientParamJSON(idClient: idClient, accessToken: accessToken, paramName: paramName, paramValue: phone)
            
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultClientQueryV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultClientQueryV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultClientQueryV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }

    
// /// Решение еще не принятно. Пока стандартная схема   Принимаем стратегию, что регистрируем сразу устройство, чтобы можно было отправлять уведомления. Регистрируем с пустым GUID. Это означает, что это еще не клиенты, а лиды.
    public func addDevice(accessToken: String, idClient: String, handler: @escaping (ResultAddDeviceV3) -> Void)
    {
        let url = URL(string: host + "/device")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            let ParamsBody = ClientParamJSON(idClient: idClient, accessToken: accessToken, paramName: "device", paramValue: "")
            
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultAddDeviceV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultAddDeviceV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultAddDeviceV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }
    
    
    
    
    public func addPersonalInfo(accessToken: String, handler: @escaping (ResultClientQueryV3) -> Void)
    {
        let url = URL(string: host + "/personalinfo")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            var param: [String: Any] = ["AccessToken": accessToken]

            let userSettings = UserSettings()

            var ClientData: [String: String] = ["Patronymic":""]
            ClientData["Name"] = userSettings.internalClient?.name
            ClientData["Soname"] = userSettings.internalClient?.soname
            ClientData["Sex"] = userSettings.internalClient?.sex.description
            if let birth = userSettings.internalClient?.dateOfBirth {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let stringDate = formatter.string(from: birth)
                ClientData["DateOfBirth"] = stringDate
            }
            
            param["ClientData"] = ClientData
            
            let jsonData = try JSONSerialization.data(withJSONObject: param, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
            print (jsonString)
            
            //let jsonData = try JSONEncoder().encode(param)
            
            //let jsonString = String(data: jsonData, encoding: .utf8)
            //print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultClientQueryV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultClientQueryV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultClientQueryV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }

    

    

    
    public func checkExistsClientBy(phoneNumber: String, accessToken: String, handler: @escaping (ResultClientQueryV3) -> Void) {
        
        let url = URL(string: host + "/\(accessToken)/phone/\(phoneNumber)")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
    
        let task = URLSession.shared.dataTask(with: request) {  (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .formatted(IPBSettings.shared.formatter)
                        
                        
                        let result = try decoder.decode(ResultClientQueryV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultClientQueryV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultClientQueryV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
    }
    
    
    

    
    
    public func getAccessToken(handler: @escaping (ResultAuthV3) -> Void)
    {
        let url = URL(string: host + "/accesstoken")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            let userSettings = UserSettings()
            
            let ParamsBody = ClientParamJSONWhithGeo(idClient: userSettings.userID, accessToken: "", paramName: "iddevice", paramValue: userSettings.deviceID, latitude: ClientLocation.main.latitude, longitude: ClientLocation.main.longitude, sourceQuery: 0)
            
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultAuthV3.self, from: data)
                        DispatchQueue.main.async {
                            handler(result)
                        }
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultAuthV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultAuthV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }
    
    
    public struct DeviceParameters: Codable
{
    public var AccessToken: String
    public var IDDivice: String
    public var IDClient: String
    public var DeviceToken: String

}
    
    
    public func updateDeviceToken(accessToken: String, deviceToken: String,  handler: @escaping (ResultClientQueryV3) -> Void)
    {
        let url = URL(string: host + "/devicetoken")
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        
        
        do
        {
            let userSettings = UserSettings()
            
            let ParamsBody = DeviceParameters(AccessToken: accessToken, IDDivice: userSettings.deviceID, IDClient: userSettings.userID, DeviceToken: deviceToken)
            
            let jsonData = try JSONEncoder().encode(ParamsBody)
            
            let jsonString = String(data: jsonData, encoding: .utf8)
            print(jsonString as Any)

        
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultClientQueryV3.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultClientQueryV3(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultClientQueryV3(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

         
        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
    }
    

    
    

}

