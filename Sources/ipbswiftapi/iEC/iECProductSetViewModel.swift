//
//  iECProductSetViewModel.swift
//
//  Created by Sergey Spevak on 22.03.2021.
//


import Foundation
import Combine





public class iECProductSetViewModel: ObservableObject
{
    @Published public var Artikul: String = ""
    @Published public var data: ResultProductSet = ResultProductSet()

    private var cancelableSet: Set<AnyCancellable> = []

    public init()
    {

        $Artikul
            .flatMap{(Artikul)->AnyPublisher<ResultProductSet, Never> in
                self.fetcData(ArtikulInit: Artikul)
                    .map{$0}
                    .eraseToAnyPublisher()

            }
            .assign(to: \.data, on :self)
            .store(in: &self.cancelableSet)


    }

    func fetcData(ArtikulInit: String)-> AnyPublisher<ResultProductSet, Never> {

           guard let url = URL(string: IPBSettings.shared.hostIEC + "/iecommercecore/api/v1/productset/"+ArtikulInit+"/00000000-0000-0000-0000-000000000000/size")
               else {
            return Just(ResultProductSet()).eraseToAnyPublisher()
           }


        Test(url: url)


        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")




        return URLSession.shared.dataTaskPublisher(for: request)
                .map{$0.data}
                .decode(type: ResultProductSet.self, decoder: decoder)
                .map{$0}
                .replaceError(with: ResultProductSet())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
        }

    func Test(url: URL?)
    {
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
       
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")

        do
        {


            request.httpBody = nil

            let task = URLSession.shared.dataTask(with: request) {(data, response, error) in

                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}

                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }

                do{
                    let decoder = JSONDecoder()

                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"

                    decoder.dateDecodingStrategy = .formatted(formatter)

                    let todoItemModel = try decoder.decode(ResultProductSet.self, from: data)
                    print("Response data:\n \(todoItemModel)")
                    print("todoItemModel result: \(todoItemModel.result!.status)")
                }catch let jsonErr{
                    print(jsonErr)
               }


        }
        task.resume()
        }

    }

}


