//
//  iECCatalogViewModel.swift
//
//  Created by Sergey Spevak on 15.03.2021.
//

import Foundation
import Combine
import SwiftUI




public struct CatalogResult: Codable {
    public var result: ResultOperation
    public var dataList: [CatalogDataList]
}

public struct CatalogDataList: Codable, Hashable {
    public var item: CatalogDataItem?
    public var childItems: [ChildItem]
    public var totalQuantity: Int?
    public var tottalCount: Int?
}

public struct CatalogDataItem: Codable, Hashable {
    public var idUnique: String?
    public var idEnterprise: String?
    public var catalogType: Int?
    public var formatViewProductType: Int?
    public var lastLoadDate: String?
    public var intID: Int?
    public var idParentCategory: String?
    public var name: String?
    public var description: String?
    public var urlImage: String?
    public var order: Int?

    public init() { }
}

public struct ChildItem: Codable, Hashable {
    public var item: CatalogDataItem?
    public var childItems: [ChildItem] = []

    public init() { }
}


//Template

//Created by Sergey Spevak on 20.12.2020.






public class iECCatalogViewModel: ObservableObject
{
    @Published public var accessToken: String = ""
    @Published public var dataList: [CatalogDataList] = [CatalogDataList]()



    private var cancelableSet: Set<AnyCancellable> = []

    public init()
    {

        $accessToken
            .flatMap{(accessToken)->AnyPublisher<[CatalogDataList], Never> in
                self.fetchListData(accessToken: accessToken)
                    .map{$0}
                    .eraseToAnyPublisher()

            }
            .assign(to: \.dataList, on :self)
            .store(in: &self.cancelableSet)




    }

    func fetchListData(accessToken: String)-> AnyPublisher<[CatalogDataList], Never> {

        guard let url = URL(string: IPBSettings.shared.hostIEC + "/iecommercecore/api/v1/catalog/"+accessToken)
               else {
            return Just([CatalogDataList]()).eraseToAnyPublisher()
           }


        Test(url: url)


        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")


            return URLSession.shared.dataTaskPublisher(for: request)
                .map{$0.data}
                .decode(type: CatalogResult.self, decoder: decoder)
                .map{$0.dataList}
                .replaceError(with: [CatalogDataList]())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
        }

    func Test(url: URL?)
    {
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        do
        {


            request.httpBody = nil

            let task = URLSession.shared.dataTask(with: request) {(data, response, error) in

                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}

                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }

                do{
                    let decoder = JSONDecoder()

                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"

                    decoder.dateDecodingStrategy = .formatted(formatter)

                    let todoItemModel = try decoder.decode(CatalogResult.self, from: data)
                    print("Response data:\n \(todoItemModel)")
                    print("todoItemModel result: \(todoItemModel.result.status)")
                }catch let jsonErr{
                    print(jsonErr)
               }


        }
        task.resume()
        }

    }

}



