//
//  iECModel.swift
//
//  Created by Sergey Spevak on 15.03.2021.
//

import Foundation






// MARK: - ResultProducts
public struct ResultProducts: Codable {
    public init(){}
    public var result: ResultOperation?
    public var data: ProductPage?

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case data = "data"
    }
}

// MARK: - ProductPage
public struct ProductPage: Codable, Equatable {
    public init(){}
    
    public var countItemsInPage: Int?
    public var totalPage: Int?
    public var numberCurrentPage: Int?
    public var allQuantityItemsProducts: Int?
    public var listProducts: [RGGoodsInventoryExt]?

    enum CodingKeys: String, CodingKey {
        case countItemsInPage = "countItemsInPage"
        case totalPage = "totalPage"
        case numberCurrentPage = "numberCurrentPage"
        case allQuantityItemsProducts = "allQuantityItemsProducts"
        case listProducts = "listProducts"
    }
}

// MARK: - RGGoodsInventoryExt
public struct RGGoodsInventoryExt: Codable, Hashable {
    public init(){}
    public var countInCart: Int?
    public var idUnique: String?
    public var name: String?
    public var extendedDescription: String?
    public var artikul: String?
    public var idEnterprise: String?
    public var url: String?
    public var imageDataJSON: String?
    public var additionalDataJSON: String?
    public var idCategory: String?
    public var popularOrder: Int?
    public var rating: Int?
    public var barcode: String?
    public var idDiscountBasisForBeginPrice: String?
    public var idFeature: String?
    public var nameFeature: String?
    public var defectName: String?
    public var kiz: String?
    public var additionaInfo: String?
    public var lastTimeUpdate: String?
    public var idrfNomenclatura: String?
    public var idrfShop: String?
    public var quantity: Int?
    public var beginPrice: Decimal?
    public var currentPrice: Decimal?
    public var costPrice: Decimal?
    public var costtPriceBasicUnit: Decimal?
    public var idrfPlace: String?

    public func getListURLImages()->[URL]
    {
        return IPBGeneralOperation.shared.getListURLImages(imageDataJSON: imageDataJSON)
    }
    
    enum CodingKeys: String, CodingKey {
        case countInCart = "countInCart"
        case idUnique = "idUnique"
        case name = "name"
        case extendedDescription = "extendedDescription"
        case artikul = "artikul"
        case idEnterprise = "idEnterprise"
        case url = "url"
        case imageDataJSON = "imageDataJSON"
        case additionalDataJSON = "additionalDataJSON"
        case idCategory = "idCategory"
        case popularOrder = "popularOrder"
        case rating = "rating"
        case barcode = "barcode"
        case idDiscountBasisForBeginPrice = "idDiscountBasisForBeginPrice"
        case idFeature = "idFeature"
        case nameFeature = "nameFeature"
        case defectName = "defectName"
        case kiz = "kiz"
        case additionaInfo = "additionaInfo"
        case lastTimeUpdate = "lastTimeUpdate"
        case idrfNomenclatura = "idrfNomenclatura"
        case idrfShop = "idrfShop"
        case quantity = "quantity"
        case beginPrice = "beginPrice"
        case currentPrice = "currentPrice"
        case costPrice = "costPrice"
        case costtPriceBasicUnit = "costtPriceBasicUnit"
        case idrfPlace = "idrfPlace"
    }
}





// MARK: - ResultProductSet
public struct ResultProductSet: Codable {
    public init(){}
    public var result: ResultOperation?
    public var data: ProductSet?

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case data = "data"
    }
}

// MARK: - ResultProductSet
public struct ProductSet: Codable {
    public init(){}
    public var dataList: [RGGoodsInventoryExt]?
    public var listDataSets: [SetData]?

    enum CodingKeys: String, CodingKey {
        case dataList = "dataList"
        case listDataSets = "listDataSets"
    }
}


// MARK: - ResultProductSet
public struct SetData: Codable, Hashable {
    public init(){}
    public var idrgGoodsInventory: String?
    public var name: String?

    enum CodingKeys: String, CodingKey {
        case idrgGoodsInventory = "idrgGoodsInventory"
        case name = "name"
    }
}
