//
//  IPBMessageChatViewModel.swift
//
//

import Foundation
import Combine

public class MessageChatViewModel: ObservableObject{
    
    @Published public var msg: SendMessageParam = SendMessageParam()
    @Published public var MsgList:[MessageData] = [MessageData]()
    @Published public var ResultMsgList:MessagesDataModel = MessagesDataModel(result: ResultOperation.NetworkError())
    
    public var dialogId:String = ""
    let host = "http://84.201.188.117:5093/messenger/mobile"
    private var cancellableSet: Set<AnyCancellable> = []
    
    public init(){
        $msg
            .flatMap {(msg) -> AnyPublisher<MessagesDataModel, Never> in
                return self.sendMsg(msg:msg)
            }
            .assign(to: \.ResultMsgList, on: self)
            .store(in: &self.cancellableSet)
    }
    public func GetInfo(accessToken:String){
        createGetDialog(accessToken: accessToken, param: CreateDialogParams())
            .assign(to: \.ResultMsgList, on: self)
            .store(in: &self.cancellableSet)
    }
    public func GetListMsg(dialogId:String){
        fetchMsgDataList(dialogID: dialogId, currentPage: 0)
            .assign(to: \.ResultMsgList, on: self)
            .store(in: &self.cancellableSet)
    }
    struct APIConstants {
        static let jsonDecoder: JSONDecoder = {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategyFormatters = [ DateFormatter.standardT,
                                                       DateFormatter.standard]
            return decoder
        }()
    }
    func makeRequest(url: URL, httpMethod: String, jsonData:Data? = nil)->URLRequest{
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod

        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        if jsonData != nil{
            request.httpBody = jsonData
        }
        return request
    }
    func fetch<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, Error> {
        URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data}
            .decode(type: T.self, decoder: APIConstants.jsonDecoder)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
   
    func createGetDialog(accessToken:String, param: CreateDialogParams)-> AnyPublisher<MessagesDataModel, Never> {
        guard let url = URL(string: host + "/dialog/cliententerprise?AccessToken=" + accessToken)
        else {
            return Just(MessagesDataModel(result: ResultOperation.NetworkError())).eraseToAnyPublisher()
        }
        let jsonData = try! JSONEncoder().encode(param)
        let request = makeRequest(url: url, httpMethod: "POST", jsonData: jsonData)
        return fetch(request)
            .map { (response: DialogDataModel) -> DialogDataModel in response}
            .replaceError(with: DialogDataModel(result: ResultOperation.NetworkError()))
            .flatMap {dialog in
                self.fetchMsgDataList(dialogID: dialog.data!.idUnique!, currentPage: 0)
            }
            .eraseToAnyPublisher()
    }
    func fetchMsgDataList(dialogID: String, currentPage: Int)-> AnyPublisher<MessagesDataModel, Never> {
        self.dialogId = dialogID
        guard let url = URL(string: host + "/messages/\(dialogID)/\(currentPage)")
        else {
            return Just(MessagesDataModel(result: ResultOperation.NetworkError())).eraseToAnyPublisher()
        }
        let request = makeRequest(url: url, httpMethod: "GET")
        return fetch(request)
            .map { (response: MessagesDataModel) -> MessagesDataModel in response}
            .replaceError(with: MessagesDataModel(result: ResultOperation.NetworkError()))
            .eraseToAnyPublisher()
    }
    func sendMsg(msg:SendMessageParam)-> AnyPublisher<MessagesDataModel, Never> {
        guard let url = URL(string: host + "/messages/text")
        else {
            return Just(MessagesDataModel(result: ResultOperation.NetworkError())).eraseToAnyPublisher()
        }
        let jsonData = try! JSONEncoder().encode(msg)
        let request = makeRequest(url: url, httpMethod: "POST", jsonData: jsonData)
        return fetch(request)
            .map { (response: MessagesDataModel) -> MessagesDataModel in response}
            .replaceError(with: MessagesDataModel(result: ResultOperation.NetworkError()))
            .eraseToAnyPublisher()
    }
}

// MARK: - Структуры для получения диалогов
struct DialogDataModel:Codable {
    var result: ResultOperation
    var data: DialogData?
    public init(result: ResultOperation)
    {
        self.result = result
    }
}

public struct DialogData:Codable {
    public var idUnique, idEnterprise, dateCreate, dataListDescription: String?
    public var additionalDataJSON, additionalData: String?
}

//MARK: - Структуры для создания диалога

public struct CreateDialogParams:Codable {
    public var descriptionDialog = ""
}

//MARK: - Структуры для отправки сообщения

public struct SendMessageParam: Codable {
    public init(idrgDialog: String = "", accessToken: String = "", contentText: String = "") {
        self.idrgDialog = idrgDialog
        self.accessToken = accessToken
        self.contentText = contentText
    }
    
    public var idrgDialog: String = ""
    public var accessToken: String = ""
    public var contentText: String = ""
}


// MARK: - Структуры для получения сообщений

public struct MessagesDataModel: Codable {
    public var result: ResultOperation
    public var dataList: [MessageData]?
    public var countPage: Int?
    public init(result: ResultOperation)
    {
        self.result = result
    }
}

public struct MessageData: Codable, Equatable {
    public var idUnique: String?
    public var idEnterprise: String?
    public var idClient: String?
    public var dataClientJSONData: String?
    public var idDialog: String?
    public var timeDisappearing: Bool?
    public var agileDataQuotedMessage: String?
    public var contentText: String?
    public var agileMediaContent: String?
    public var dateCreate: Date?
    public var dateRead: Date?
    public var agileAttachmentContent: String?
    public var agileListReactionData: String?
    public var dateChange: Date?
    public var idLastRRGHistory: String?
}
