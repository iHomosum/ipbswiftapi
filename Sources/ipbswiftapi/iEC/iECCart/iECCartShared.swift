//
//  CartViewModel.swift
//
//  Created by Sergey Spevak on 17.03.2021.
//

import Foundation
import Combine


public class iECCartShared
{
    
    public static var shared: iECCartShared = iECCartShared()
    
    let host = IPBSettings.shared.hostIEC + "/iecommercecore/api/v3/cart"
    
   
    func getDefaultDecoder()->JSONDecoder
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        return decoder
    }
    
    
    //    post_{AccessToken}/confirmorder/
    public func fetchConfirmOrder(accessToken: String, handler: @escaping (ResultCart)->Void)
    {
        
        let url = URL(string: host + "/" + accessToken + "/confirmorder" )
        let request = makeRequest(url: url!, httpMethod: "POST")
        
        do
        {

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultCart.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultCart(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultCart(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
        
        
    }
    
    //    post_{AccessToken}/post/
    public func fetchPostOrder(accessToken: String, handler: @escaping (ResultCart)->Void)
    {
        
        let url = URL(string: host + "/" + accessToken + "/post" )
        let request = makeRequest(url: url!, httpMethod: "POST")
        
        do
        {

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultCart.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultCart(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultCart(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
        
        
    }
    
    

    
//    //    post_{AccessToken}/cancelimplementbonus
//    func fetchCancelImplementBonus(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
//
//        guard let url = URL(string: host + "/" + params.accessToken + "/cancelimplementbonus" )
//               else {
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
//           }
//
//        let requelkst = makeRequest(url: url, httpMethod: "POST")
//        return execute(forRequest: request)
//    }
//
//    //    post_{AccessToken}/implementbonus {ParamImplementBonusV3}
//    func fetchImplementBonus(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
//
//        guard let url = URL(string: host + "/" + params.accessToken + "/implementbonus")
//               else {
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
//           }
//
//        var request = makeRequest(url: url, httpMethod: "POST")
//
//        do
//        {
//            let ParamsBody = params.paramImplementBonusV3
//            let jsonData = try JSONEncoder().encode(ParamsBody)
//            request.httpBody = jsonData
//
//            return execute(forRequest: request)
//        }
//        catch
//        {
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
//        }
//    }
//
//
//    //    post_{AccessToken}/removenomenklaturefromcart/{IDRFNomenclature}
//    func fetchRemoveNomenklatureFromCart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
//
//        guard let url = URL(string: host + "/" + params.accessToken + "/removedrfromcart/" + params.IDRFNomenclature!.description)
//               else {
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
//           }
//
//        let request = makeRequest(url: url, httpMethod: "POST")
//        return execute(forRequest: request)
//    }
//
//    func fetchRemovedrfromcart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
//
//        guard let url = URL(string: host + "/" + params.accessToken + "/removedrfromcart/" + params.IDDRSaleRow!.description )
//               else {
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
//           }
//
//        let request = makeRequest(url: url, httpMethod: "POST")
//        return execute(forRequest: request)
//    }
    
    
    
//    func fetchGetCart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
//
//        guard let url = URL(string: host + "/" + params.accessToken)
//               else {
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
//           }
//
//
//        let request = makeRequest(url: url, httpMethod: "GET")
//
//
//        return execute(forRequest: request)
//    }
//
    public func fetchAddToCart(accessToken: String, params: ParamGoodsToECommers, handler: @escaping (ResultCart)->Void)
    {
        
        guard let url = URL(string: host + "/" + accessToken + "/addtocart")
               else {
                    handler(ResultCart(result: ResultOperation.NetworkError()))
                    return
           }
        
        var request = makeRequest(url: url, httpMethod: "POST")
        
        
        do
        {
            let jsonData = try JSONEncoder().encode(params)
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
                if let jsonString = String(data: data, encoding: .utf8) {
                            print(jsonString)
                }

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultCart.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultCart(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultCart(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
        
    }
    
    
    
    
    
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest
    {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    

}



    

//post_{AccessToken}/deliveryremove
//post_{AccessToken}/delivery {DeliveryData}

//ResultOperation post_{AccessToken}/address {ParamForAddAddressV3}
//ResultOperation post_{AccessToken}/comment ParamForAndComment

//ResultOperation post_{AccessToken}/goods/count {ParamGoodsToECommers}
//ResultOperation post_{AccessToken}/goods/count/remove {ParamGoodsToECommers}
//ResultCountInCart get_{AccessToken}/goods/{IDRGGoodsInventory}


