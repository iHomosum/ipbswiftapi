//
//  CartViewModel.swift
//
//  Created by Sergey Spevak on 17.03.2021.
//

import Foundation
import Combine

public struct ParamGoodsToECommers: Encodable
{
    public init(IDGoodsInventory: String, Count: Int, IDSellerAmbassador: UUID?)
    {
        self.IDGoodsInventory = IDGoodsInventory
        self.Count = Count
        self.IDSellerAmbassador = IDSellerAmbassador
    }
    
    ///<summary>Идентификатор регистра товара конкретного магазина</summary>
    public var IDGoodsInventory: String

    ///<summary>Количество добавляемого товара</summary>
    public var Count: Int
    
    public var IDSellerAmbassador: UUID?

}

public struct ParamImplementBonusV3: Encodable
{
    public init(){}
    public var SumPaymentBonus: Decimal = 0
}
  
public struct DeliveryData: Encodable
{
    public init(){}
      ///<summary>Идентификатор регистра товаров для данной службы доставки</summary>
    public var IDRGGoodsInventory: String = ""

      ///<summary>Наименование, которое показывается клиенту</summary>
    public var DisplayName: String = ""

      ///<summary>Рассчитанная цена</summary>
    public var CalculatedPrice: Decimal = 0
    
      ///<summary>Комментарий</summary>
    public var Comment: String = ""
}

public struct ParamForAddAddressV3: Encodable
{
    public init(idAdress: String, adressString: String){
        self.IDAddress = idAdress
        self.AdressString = adressString
    }
    
    public var IDAddress: String
    public var AdressString: String
 }


public struct ParamForAddComment: Encodable
{
    public init(comment: String){
        self.Comment = comment
    }
    public var Comment: String
}



public class ParamsCommandCart: ObservableObject
{
    public init(){}
    
    @Published public var command: commandCartType = commandCartType.get
    public var accessToken: String = ""
    @Published public var paramGoodsToECommers: ParamGoodsToECommers?
    @Published public var paramImplementBonusV3: ParamImplementBonusV3?
    @Published public var paramForAddAddressV3: ParamForAddAddressV3?
    @Published public var paramForAddComment: ParamForAddComment?
    @Published public var IDDRSaleRow: UUID?
    @Published public var IDRFNomenclature: UUID?
}


public enum commandCartType {
    case get
    case addtocart
    case removedrfromcart
    case removenomenklaturefromcart
    case implementbonus
    case cancelimplementbonus
    case confirmorder
    case addaddress
    case addcomment
    case clear
    
//    get_{AccessToken}
//    post_{AccessToken}/addtocart {ParamGoodsToECommers}
//    post_{AccessToken}/removedrfromcart/{IDDRSaleRow}
//    post_{AccessToken}/removenomenklaturefromcart/{IDRFNomenclature}
//    psot_{AccessToken}/implementbonus {ParamImplementBonusV3}
//    post_{AccessToken}/cancelimplementbonus
//    post_{AccessToken}/confirmorder
//
}


public class iECCartViewModel: ObservableObject
{
    @Published public var paramsCommandCart: ParamsCommandCart = ParamsCommandCart()
    //@Published var accessToken: String = ""
    
    
    let host = IPBSettings.shared.hostIEC + "/iecommercecore/api/v3/cart"
    
    @Published public var data: ExtDHSaleHead = ExtDHSaleHead()
    
    
    func getDefaultDecoder()->JSONDecoder
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        return decoder
    }
    
    

    private var cancelableSet: Set<AnyCancellable> = []

    public init()
    {
        $paramsCommandCart
            .flatMap{(paramsCommandCart)->AnyPublisher<ExtDHSaleHead, Never> in
                self.fetchData(params: paramsCommandCart)
                    .map{$0}
                    .eraseToAnyPublisher()
            }
            .assign(to: \.data, on :self)
            .store(in: &self.cancelableSet)
    }
    
    func fetchData(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        switch params.command {
        case commandCartType.get:
            return fetchGetCart(params: params)
        case commandCartType.addtocart:
            return fetchAddToCart(params: params)
        case commandCartType.removedrfromcart:
            return fetchRemovedrfromcart(params: params)
        case commandCartType.removenomenklaturefromcart:
            return fetchRemoveNomenklatureFromCart(params: params)
        case commandCartType.implementbonus:
            return fetchImplementBonus(params: params)
        case commandCartType.cancelimplementbonus:
            return fetchCancelImplementBonus(params: params)
        case commandCartType.confirmorder:
            return fetchConfirmOrder(params: params)
        case commandCartType.addaddress:
            return fetchAddAdress(params: params)
        case commandCartType.addcomment:
            return fetchAddComment(params: params)
            
        case commandCartType.clear:
            return fetchClear(params: params)
//        default:
//            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
        }
        
    }
    
    //ResultOperation post_{AccessToken}/address {ParamForAddAddressV3}
    func fetchAddAdress(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/address" )
        else { return Just(ExtDHSaleHead()).eraseToAnyPublisher() }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    //ResultOperation post_{AccessToken}/comment ParamForAndComment
    func fetchAddComment(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/comment" )
        else { return Just(ExtDHSaleHead()).eraseToAnyPublisher() }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    //    post_{AccessToken}/confirmorder/
    func fetchConfirmOrder(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/confirmorder" )
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    
    //    post_{AccessToken}/clear/
    func fetchClear(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/clear" )
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    
    //    post_{AccessToken}/cancelimplementbonus
    func fetchCancelImplementBonus(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/cancelimplementbonus" )
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    //    post_{AccessToken}/implementbonus {ParamImplementBonusV3}
    func fetchImplementBonus(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/implementbonus")
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        var request = makeRequest(url: url, httpMethod: "POST")

        do
        {
            let ParamsBody = params.paramImplementBonusV3
            let jsonData = try JSONEncoder().encode(ParamsBody)
            request.httpBody = jsonData

            return execute(forRequest: request)
        }
        catch
        {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
        }
    }
    
    //    post_{AccessToken}/removenomenklaturefromcart/{IDRFNomenclature}
    func fetchRemoveNomenklatureFromCart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/removenomenklaturefromcart/" + params.IDRFNomenclature!.description)
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    func fetchRemovedrfromcart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/removedrfromcart/" + params.IDDRSaleRow!.description )
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        let request = makeRequest(url: url, httpMethod: "POST")
        return execute(forRequest: request)
    }
    
    func fetchGetCart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken)
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }


        let request = makeRequest(url: url, httpMethod: "GET")


        return execute(forRequest: request)
    }
    
    func fetchAddToCart(params: ParamsCommandCart)-> AnyPublisher<ExtDHSaleHead, Never> {
        
        guard let url = URL(string: host + "/" + params.accessToken + "/addtocart")
               else {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
           }
        
        var request = makeRequest(url: url, httpMethod: "POST")
        
        do
        {
            //let ParamsBody = params.paramGoodsToECommers
            let jsonData = try JSONEncoder().encode(params.paramGoodsToECommers)
            request.httpBody = jsonData
            
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print(jsonString)
            }

            return execute(forRequest: request)
        }
        catch
        {
            return Just(ExtDHSaleHead()).eraseToAnyPublisher()
        }
    }
    
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest
    {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    
    func execute(forRequest: URLRequest)->AnyPublisher<ExtDHSaleHead, Never>
    {
        return URLSession.shared.dataTaskPublisher(for: forRequest)
            .map{$0.data}
            .decode(type: ResultCart.self, decoder: getDefaultDecoder())
            .map{$0.data!}
            .replaceError(with: ExtDHSaleHead())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
        
    
    
}

//post_{AccessToken}/deliveryremove
//post_{AccessToken}/delivery {DeliveryData}

//ResultOperation post_{AccessToken}/goods/count {ParamGoodsToECommers}
//ResultOperation post_{AccessToken}/goods/count/remove {ParamGoodsToECommers}
//ResultCountInCart get_{AccessToken}/goods/{IDRGGoodsInventory}

public struct ResultCountInCart
{
    ///<summary>Result</summary>
    var result: ResultOperation
    var data: DataRowInCart
    
}

public struct DataRowInCart
{
    var Count: Int
    var IDRGGoodsInventoty: UUID
}
