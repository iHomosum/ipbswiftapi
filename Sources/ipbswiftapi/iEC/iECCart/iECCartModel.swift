// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let resultProducts = try? newJSONDecoder().decode(ResultProducts.self, from: jsonData)

import Foundation


// MARK: - ResultProducts
public struct ResultCart: Codable {
    
    public init()
    {}
    
    public init(result: ResultOperation)
    {
        self.result = result
        
    }
    
    public var result: ResultOperation?
    public var data: ExtDHSaleHead?

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case data = "data"
    }
}

// MARK: - DataClass
public struct ExtDHSaleHead: Codable, Hashable {
    public init()
    {}
    
    public var adAvailablePaymentBonus: Int?
    public var idUnique: String?
    public var idEnterprise: String?
    public var idExternalERP: String?
    public var dateDoc: String?
    public var number: String?
    public var fDocumentNumber: Int?
    public var adressString: String?
    public var idEXTDataAddress: String?
    public var commentClient: String?
    public var posted: String?
    public var idPreSale: String?
    public var currentPositionInCashCahnge: Int?
    public var typeSaleReturn: Int?
    public var idrfShop: String?
    public var idrgCashChange: String?
    public var idrfCashier: String?
    public var idBayer: String?
    public var beginTimeService: String?
    public var endTimeService: String?
    public var mode: Int?
    public var statusOrder: Int?
    public var statusObject: Int?
    public var postedAddBonuses: String?
    public var postedWritingBonuses: String?
    public var dateSync: String?
    public var dateSyncWithExternalERP: String?
    public var indexCount: Int?
    public var fiscalCheckNumber: Int?
    public var dateToSend: String?
    public var dateConfirm: String?
    public var dateCollection: String?
    public var dateTranssferToSend: String?
    public var dateStartProcessingDelivery: String?
    public var dateCustomerReceived: String?
    public var dateClose: String?
    public var levelOfSatisfaction: Int?
    public var reasonLevelOfSatisfaction: String?
    public var systemLevelOfSatisfaction: Int?
    public var idEmployyeOrderPicker: String?
    public var idEmployyeCourier: String?
    public var drDiscountSaleHead: [DRDiscountSaleHead]?
    public var drGiftCardPayment: [DRGiftCardPayment]?
    public var drPaymentSaleHead: [DRPaymentSaleHead]?
    public var drSaleRow: [DRSaleRow]?
    public var drMetaDHSaleHead: [DRMetaDHSaleHead]?
    public var drComment: [DRComment]?
    
    
    public func sumTotalEnd()->Decimal
    {
        if drSaleRow == nil
        {
            return 0
        }
        var SumTotal: Decimal = 0
        for item in drSaleRow!
        {
            SumTotal = SumTotal + item.endPrice!
        }
        return SumTotal
    }
    
    public func sumTotalBegin()->Decimal
    {
        if drSaleRow == nil
        {
            return 0
        }
        var SumTotal: Decimal = 0
        for item in drSaleRow!
        {
            SumTotal = SumTotal + item.basePrice!
        }
        return SumTotal
    }
    
    public func sumBaseDiscount()->Decimal
    {
        return sumTotalBegin() - sumTotalEnd()
    }
    
    public func sumAddedBonuses()->Decimal
    {
        if drSaleRow == nil
        {
            return 0
        }
        var SumTotal: Decimal = 0
        for item in drSaleRow!
        {
            if item.drAddedBonus == nil
            {
                continue
            }
            for itemAB in item.drAddedBonus!
            {
                SumTotal = SumTotal + itemAB.sumBonus!
            }
        }
        return SumTotal
    }
    
    public func sumPaymentBonuses()->Decimal
    {
        if drSaleRow == nil
        {
            return 0
        }
        var SumTotal: Decimal = 0
        for item in drSaleRow!
        {
            if item.drPaymentRowBonus == nil
            {
                continue
            }
            for itemAB in item.drPaymentRowBonus!
            {
                SumTotal = SumTotal + itemAB.sumBonus!
            }
        }
        return SumTotal
    }
    
    public func sumAdditioalDiscount()->Decimal
    {
        if drSaleRow == nil
        {
            return 0
        }
        var SumTotal: Decimal = 0
        for item in drSaleRow!
        {
            if item.drDiscountPosition == nil
            {
                continue
            }
            for itemAB in item.drDiscountPosition!
            {
                SumTotal = SumTotal + itemAB.sumDiscount!
            }
        }
        return SumTotal
    }
    
    

    enum CodingKeys: String, CodingKey {
        case adAvailablePaymentBonus = "adAvailablePaymentBonus"
        case idUnique = "idUnique"
        case idEnterprise = "idEnterprise"
        case idExternalERP = "idExternalERP"
        case dateDoc = "dateDoc"
        case number = "number"
        case fDocumentNumber = "fDocumentNumber"
        case adressString = "adressString"
        case idEXTDataAddress = "idExtDataAddress"
        case commentClient = "commentClient"
        case posted = "posted"
        case idPreSale = "idPreSale"
        case currentPositionInCashCahnge = "currentPositionInCashCahnge"
        case typeSaleReturn = "typeSaleReturn"
        case idrfShop = "idrfShop"
        case idrgCashChange = "idrgCashChange"
        case idrfCashier = "idrfCashier"
        case idBayer = "idBayer"
        case beginTimeService = "beginTimeService"
        case endTimeService = "endTimeService"
        case mode = "mode"
        case statusOrder = "statusOrder"
        case statusObject = "statusObject"
        case postedAddBonuses = "postedAddBonuses"
        case postedWritingBonuses = "postedWritingBonuses"
        case dateSync = "dateSync"
        case dateSyncWithExternalERP = "dateSyncWithExternalERP"
        case indexCount = "indexCount"
        case fiscalCheckNumber = "fiscalCheckNumber"
        case dateToSend = "dateToSend"
        case dateConfirm = "dateConfirm"
        case dateCollection = "dateCollection"
        case dateTranssferToSend = "dateTranssferToSend"
        case dateStartProcessingDelivery = "dateStartProcessingDelivery"
        case dateCustomerReceived = "dateCustomerReceived"
        case dateClose = "dateClose"
        case levelOfSatisfaction = "levelOfSatisfaction"
        case reasonLevelOfSatisfaction = "reasonLevelOfSatisfaction"
        case systemLevelOfSatisfaction = "systemLevelOfSatisfaction"
        case idEmployyeOrderPicker = "idEmployyeOrderPicker"
        case idEmployyeCourier = "idEmployyeCourier"
        case drDiscountSaleHead = "drDiscountSaleHead"
        case drGiftCardPayment = "drGiftCardPayment"
        case drPaymentSaleHead = "drPaymentSaleHead"
        case drSaleRow = "drSaleRow"
        case drMetaDHSaleHead = "drMetaDHSaleHead"
        case drComment = "drComment"
    }
}

// MARK: - DRComment
public struct DRComment: Codable, Hashable {
    
    public init()
    {}
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var dateComment: String?
    public var idUser: String?
    public var userString: String?
    public var comment: String?
    public var messageForClient: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case dateComment = "dateComment"
        case idUser = "idUser"
        case userString = "userString"
        case comment = "comment"
        case messageForClient = "messageForClient"
    }
}

// MARK: - DRDiscountSaleHead
public struct DRDiscountSaleHead: Codable, Hashable {
    public init()
    {}
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var idrfTypeDiscount: String?
    public var sumDiscount: Int?
    public var reasonDiscounts: String?
    public var dhSaleHead: DhSaleHead?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case idrfTypeDiscount = "idrfTypeDiscount"
        case sumDiscount = "sumDiscount"
        case reasonDiscounts = "reasonDiscounts"
        case dhSaleHead = "dhSaleHead"
    }
}

// MARK: - DhSaleHead
public struct DhSaleHead: Codable, Hashable {
    public init()
    {}
    
    public var idUnique: String?
    public var idEnterprise: String?
    public var idExternalERP: String?
    public var dateDoc: String?
    public var number: String?
    public var fDocumentNumber: Int?
    public var adressString: String?
    public var idEXTDataAddress: String?
    public var commentClient: String?
    public var posted: String?
    public var idPreSale: String?
    public var currentPositionInCashCahnge: Int?
    public var typeSaleReturn: Int?
    public var idrfShop: String?
    public var idrgCashChange: String?
    public var idrfCashier: String?
    public var idBayer: String?
    public var beginTimeService: String?
    public var endTimeService: String?
    public var mode: Int?
    public var statusOrder: Int?
    public var statusObject: Int?
    public var postedAddBonuses: String?
    public var postedWritingBonuses: String?
    public var dateSync: String?
    public var dateSyncWithExternalERP: String?
    public var indexCount: Int?
    public var fiscalCheckNumber: Int?
    public var dateToSend: String?
    public var dateConfirm: String?
    public var dateCollection: String?
    public var dateTranssferToSend: String?
    public var dateStartProcessingDelivery: String?
    public var dateCustomerReceived: String?
    public var dateClose: String?
    public var levelOfSatisfaction: Int?
    public var reasonLevelOfSatisfaction: String?
    public var systemLevelOfSatisfaction: Int?
    public var idEmployyeOrderPicker: String?
    public var idEmployyeCourier: String?
    public var drDiscountSaleHead: [JSONNull?]?
    public var drGiftCardPayment: [JSONNull?]?
    public var drPaymentSaleHead: [JSONNull?]?
    public var drSaleRow: [JSONNull?]?
    public var drMetaDHSaleHead: [DRMetaDHSaleHead]?
    public var drComment: [DRComment]?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case idEnterprise = "idEnterprise"
        case idExternalERP = "idExternalERP"
        case dateDoc = "dateDoc"
        case number = "number"
        case fDocumentNumber = "fDocumentNumber"
        case adressString = "adressString"
        case idEXTDataAddress = "idExtDataAddress"
        case commentClient = "commentClient"
        case posted = "posted"
        case idPreSale = "idPreSale"
        case currentPositionInCashCahnge = "currentPositionInCashCahnge"
        case typeSaleReturn = "typeSaleReturn"
        case idrfShop = "idrfShop"
        case idrgCashChange = "idrgCashChange"
        case idrfCashier = "idrfCashier"
        case idBayer = "idBayer"
        case beginTimeService = "beginTimeService"
        case endTimeService = "endTimeService"
        case mode = "mode"
        case statusOrder = "statusOrder"
        case statusObject = "statusObject"
        case postedAddBonuses = "postedAddBonuses"
        case postedWritingBonuses = "postedWritingBonuses"
        case dateSync = "dateSync"
        case dateSyncWithExternalERP = "dateSyncWithExternalERP"
        case indexCount = "indexCount"
        case fiscalCheckNumber = "fiscalCheckNumber"
        case dateToSend = "dateToSend"
        case dateConfirm = "dateConfirm"
        case dateCollection = "dateCollection"
        case dateTranssferToSend = "dateTranssferToSend"
        case dateStartProcessingDelivery = "dateStartProcessingDelivery"
        case dateCustomerReceived = "dateCustomerReceived"
        case dateClose = "dateClose"
        case levelOfSatisfaction = "levelOfSatisfaction"
        case reasonLevelOfSatisfaction = "reasonLevelOfSatisfaction"
        case systemLevelOfSatisfaction = "systemLevelOfSatisfaction"
        case idEmployyeOrderPicker = "idEmployyeOrderPicker"
        case idEmployyeCourier = "idEmployyeCourier"
        case drDiscountSaleHead = "drDiscountSaleHead"
        case drGiftCardPayment = "drGiftCardPayment"
        case drPaymentSaleHead = "drPaymentSaleHead"
        case drSaleRow = "drSaleRow"
        case drMetaDHSaleHead = "drMetaDHSaleHead"
        case drComment = "drComment"
    }
}

// MARK: - DRMetaDHSaleHead
public struct DRMetaDHSaleHead: Codable, Hashable {
    public init()
    {}
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var key: String?
    public var value: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case key = "key"
        case value = "value"
    }
}

// MARK: - DRGiftCardPayment
public struct DRGiftCardPayment: Codable, Hashable {
    public init()
    {}
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var typeCard: Int?
    public var numberCard: Int?
    public var sumInCard: Int?
    public var sumPayment: Int?
    public var dhSaleHead: DhSaleHead?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case typeCard = "typeCard"
        case numberCard = "numberCard"
        case sumInCard = "sumInCard"
        case sumPayment = "sumPayment"
        case dhSaleHead = "dhSaleHead"
    }
}

// MARK: - DRPaymentSaleHead
public struct DRPaymentSaleHead: Codable, Hashable {
    public init()
    {}
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var idrfTypePaymentMethod: String?
    public var relationData: String?
    public var sumPayment: Int?
    public var dhSaleHead: DhSaleHead?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case idrfTypePaymentMethod = "idrfTypePaymentMethod"
        case relationData = "relationData"
        case sumPayment = "sumPayment"
        case dhSaleHead = "dhSaleHead"
    }
}

// MARK: - DRSaleRow
public struct DRSaleRow: Codable, Hashable {
    public init()
    {}
    
    public static func == (lhs: DRSaleRow, rhs: DRSaleRow) -> Bool {
        if lhs.idUnique == rhs.idUnique
        {
            return true
        }
        return false
    }
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var iddrSaleForReturn: String?
    public var iddrRetrun: String?
    public var idrfNomnclatura: String?
    public var idrgGoodsInventory: String?
    public var idFeature: String?
    public var barcodeCodeGoods: String?
    public var nameGoods: String?
    public var basePrice: Decimal?
    public var idDiscountBasisForBeginPrice: String?
    public var beginPrice: Decimal?
    public var endPrice: Decimal?
    public var priceCost: Int?
    public var idSeller: String?
    public var indexInCheck: Int?
    public var saleType: Int?
    public var defectName: String?
    public var idrfReasonReturn: String?
    public var reasonReturnText: String?
    public var kiz: String?
    public var imageJSONData: String?
    public var datePickingFirst: String?
    public var datePickingSecond: String?
    public var drAddedBonus: [DRAddedBonus]?
    public var drDiscountPosition: [DRDiscountPosition]?
    public var drPaymentPositionSale: [DRPaymentPositionSale]?
    public var drPaymentRowBonus: [DRPaymentRowBonus]?
    public var dhSaleHead: DhSaleHead?
    
    

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case iddrSaleForReturn = "iddrSaleForReturn"
        case iddrRetrun = "iddrRetrun"
        case idrfNomnclatura = "idrfNomnclatura"
        case idrgGoodsInventory = "idrgGoodsInventory"
        case idFeature = "idFeature"
        case barcodeCodeGoods = "barcodeCodeGoods"
        case nameGoods = "nameGoods"
        case basePrice = "basePrice"
        case idDiscountBasisForBeginPrice = "idDiscountBasisForBeginPrice"
        case beginPrice = "beginPrice"
        case endPrice = "endPrice"
        case priceCost = "priceCost"
        case idSeller = "idSeller"
        case indexInCheck = "indexInCheck"
        case saleType = "saleType"
        case defectName = "defectName"
        case idrfReasonReturn = "idrfReasonReturn"
        case reasonReturnText = "reasonReturnText"
        case kiz = "kiz"
        case imageJSONData = "imageJSONData"
        case datePickingFirst = "datePickingFirst"
        case datePickingSecond = "datePickingSecond"
        case drAddedBonus = "drAddedBonus"
        case drDiscountPosition = "drDiscountPosition"
        case drPaymentPositionSale = "drPaymentPositionSale"
        case drPaymentRowBonus = "drPaymentRowBonus"
        case dhSaleHead = "dhSaleHead"
    }
}

// MARK: - DRAddedBonus
public struct DRAddedBonus: Codable, Hashable {
    public init()
    {}
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var iddrSaleRow: String?
    public var idrfAction: String?
    public var idBonusType: String?
    public var nameBonusType: String?
    public var sumBonus: Decimal?
    public var idBonusLog: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case iddrSaleRow = "iddrSaleRow"
        case idrfAction = "idrfAction"
        case idBonusType = "idBonusType"
        case nameBonusType = "nameBonusType"
        case sumBonus = "sumBonus"
        case idBonusLog = "idBonusLog"
    }
}

// MARK: - DRDiscountPosition
public struct DRDiscountPosition: Codable, Hashable {
    public init()
    {}
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var iddrSaleRow: String?
    public var idrfTypeDiscount: String?
    public var typeDiscount: Int?
    public var percentValue: Int?
    public var sumValue: Decimal?
    public var sumDiscount: Decimal?
    public var reasonDiscounts: String?
    public var iddrDiscountSaleHead: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case iddrSaleRow = "iddrSaleRow"
        case idrfTypeDiscount = "idrfTypeDiscount"
        case typeDiscount = "typeDiscount"
        case percentValue = "percentValue"
        case sumValue = "sumValue"
        case sumDiscount = "sumDiscount"
        case reasonDiscounts = "reasonDiscounts"
        case iddrDiscountSaleHead = "iddrDiscountSaleHead"
    }
}

// MARK: - DRPaymentPositionSale
public struct DRPaymentPositionSale: Codable, Hashable {
    public init()
    {}
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var iddrSaleRow: String?
    public var idrfTypePaymentMethod: String?
    public var relationData: String?
    public var sumPayment: Int?
    public var iddrPaymentSaleHead: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case iddrSaleRow = "iddrSaleRow"
        case idrfTypePaymentMethod = "idrfTypePaymentMethod"
        case relationData = "relationData"
        case sumPayment = "sumPayment"
        case iddrPaymentSaleHead = "iddrPaymentSaleHead"
    }
}

// MARK: - DRPaymentRowBonus
public struct DRPaymentRowBonus: Codable, Hashable {
    public init()
    {}
    
    public var idUnique: String?
    public var iddhSaleHead: String?
    public var iddrSaleRow: String?
    public var idBonusType: Int?
    public var nameBonusType: String?
    public var idBonusLife: String?
    public var sumBonus: Decimal?
    public var idBonusLog: String?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case iddhSaleHead = "iddhSaleHead"
        case iddrSaleRow = "iddrSaleRow"
        case idBonusType = "idBonusType"
        case nameBonusType = "nameBonusType"
        case idBonusLife = "idBonusLife"
        case sumBonus = "sumBonus"
        case idBonusLog = "idBonusLog"
    }
}



// MARK: - Encode/decode helpers

public class JSONNull: Codable, Hashable {
    

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
