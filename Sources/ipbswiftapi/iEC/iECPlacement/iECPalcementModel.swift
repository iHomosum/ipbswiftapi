//
//  File.swift
//  
//
//  Created by Sergey Spevak on 20.04.2021.
//

import Foundation

public struct RFPlacement: Codable, Hashable {
    public var idUnique: String?
    public var idEnterprise: String?
    public var idrfShop: String?
    public var zoneName: String?
    public var planogrammaName: String?
    public var segmentNumber: Int?
    public var numberShelf: Int?
    public var positionOnNumber: Int?

    enum CodingKeys: String, CodingKey {
        case idUnique = "idUnique"
        case idEnterprise = "idEnterprise"
        case idrfShop = "idrfShop"
        case zoneName = "zoneName"
        case planogrammaName = "planogrammaName"
        case segmentNumber = "segmentNumber"
        case numberShelf = "numberShelf"
        case positionOnNumber = "positionOnNumber"
    }

    public init(idUnique: String?, idEnterprise: String?, idrfShop: String?, zoneName: String?, planogrammaName: String?, segmentNumber: Int?, numberShelf: Int?, positionOnNumber: Int?) {
        self.idUnique = idUnique
        self.idEnterprise = idEnterprise
        self.idrfShop = idrfShop
        self.zoneName = zoneName
        self.planogrammaName = planogrammaName
        self.segmentNumber = segmentNumber
        self.numberShelf = numberShelf
        self.positionOnNumber = positionOnNumber
    }
}

public struct ResultListPlacement: Codable
{
    public init()
    {}
    
    public init(result: ResultOperation)
    {
        self.result = result
        
    }
    
    ///<summary>Result</summary>
    public var result: ResultOperation?
    public var dataList: [RFPlacement]?
    
}

public struct ResultPlacement: Codable
{
    public init()
    {}
    
    public init(result: ResultOperation)
    {
        self.result = result
        
    }
    
    public var result: ResultOperation?
    public var data: RFPlacement?
}

