//
//  File.swift
//  
//
//  Created by Sergey Spevak on 20.04.2021.
//

import Foundation
import Combine

public class iECPlacementVieModel: ObservableObject
{
    public static var shared = iECPlacementVieModel()
    
    @Published public var idrfShop: String = ""
    
    @Published public var dataList: [RFPlacement] = [RFPlacement]()
    
    let host = IPBSettings.shared.hostIEC + "/iecommercecore/api/v1/placement"

    func getDefaultDecoder()->JSONDecoder
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        return decoder
    }
    
    private var cancelableSet: Set<AnyCancellable> = []

    public init()
    {
        $idrfShop
            .flatMap{(idrfShop)->AnyPublisher<[RFPlacement], Never> in
                self.fetchListPlacement(idrfShop: idrfShop)
                    .map{$0}
                    .eraseToAnyPublisher()

            }
            .assign(to: \.dataList, on :self)
            .store(in: &self.cancelableSet)
    }
    
    func fetchListPlacement(idrfShop: String)-> AnyPublisher<[RFPlacement], Never> {
        
        guard let url = URL(string: host + "/list/" + idrfShop)
               else {
            return Just([RFPlacement]()).eraseToAnyPublisher()
           }
        
        let request = IPBSettings.shared.makeRequest(url: url, httpMethod: "GET")
        
        do
        {
            return execute(forRequest: request)
        }
        catch
        {
            return Just([RFPlacement]()).eraseToAnyPublisher()
        }
    }
    
    func execute(forRequest: URLRequest)->AnyPublisher<[RFPlacement], Never>
    {
        return URLSession.shared.dataTaskPublisher(for: forRequest)
            .map{$0.data}
            .decode(type: ResultListPlacement.self, decoder: getDefaultDecoder())
            .map{$0.dataList!}
            .replaceError(with: [RFPlacement]())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    
    func createPlacement(item: RFPlacement, handler: @escaping (ResultPlacement)->Void)
    {
        guard let url = URL(string: host)
               else {
                    handler(ResultPlacement(result: ResultOperation.NetworkError()))
                    return
           }
        
        var request = IPBSettings.shared.makeRequest(url: url, httpMethod: "POST")
        
        
        do
        {
            let jsonData = try JSONEncoder().encode(item)
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}
                
            

                do{
                    let httpResponse = response as! HTTPURLResponse
                    print(httpResponse.statusCode)
                    
                    if httpResponse.statusCode == 200
                    {
                        let result = try JSONDecoder().decode(ResultPlacement.self, from: data)
                        handler(result)
                    }
                    
                    if httpResponse.statusCode == 403
                    {
                        handler(ResultPlacement(result: ResultOperation.Network403()))
                    }
                    
                }catch let jsonErr{
                    handler(ResultPlacement(result: ResultOperation.NetworkError()))
                    print(jsonErr)
               }

        }
        task.resume()
        }
        catch
        {
            print("Error")
        }
        
    }
        
}
