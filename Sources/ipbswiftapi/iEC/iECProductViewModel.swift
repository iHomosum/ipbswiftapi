//
//  iECProductViewModel.swift
//
//  Created by Sergey Spevak on 16.03.2021.
//


import Foundation
import Combine

public enum commandProductType {
    
    case getByCategory
    case getByIDRFGoodsInventory
    
    case notImplement
}


public class ParamsCommandProduct: ObservableObject
{
    public init(){}
    @Published public var command: commandProductType = commandProductType.notImplement
    public var accessToken: String = ""
    @Published public var idRFCategory: String = ""
    @Published public var idRFGoodsInventory: String = ""

}

public class iECProductViewModel: ObservableObject
{
    
    @Published public var paramsCommandProduct: ParamsCommandProduct = ParamsCommandProduct()
    
    @Published public var data: ProductPage = ProductPage()
    
    let host = IPBSettings.shared.hostIEC + "/iecommercecore/api/v1/products"

    func getDefaultDecoder()->JSONDecoder
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        return decoder
    }
    
    private var cancelableSet: Set<AnyCancellable> = []

    public init()
    {

        
        $paramsCommandProduct
            .flatMap{(paramsCommandProduct)->AnyPublisher<ProductPage, Never> in
                self.fetchData(params: paramsCommandProduct)
                    .map{$0}
                    .eraseToAnyPublisher()

            }
            .assign(to: \.data, on :self)
            .store(in: &self.cancelableSet)




    }
    
    
    func fetchData(params: ParamsCommandProduct)-> AnyPublisher<ProductPage, Never> {
        
        switch params.command {
        case commandProductType.getByCategory:
            return fetchByCategory(idRFCategory: params.idRFCategory, accessToken: params.accessToken)
        case commandProductType.getByIDRFGoodsInventory:
            return fetchByIDRGGoodsInventory(IDRGGoodsInventory: params.idRFGoodsInventory)
        
        case .notImplement:
            return Just(ProductPage()).eraseToAnyPublisher()
        }
        
    }
    
    
    func fetchByIDRGGoodsInventory(IDRGGoodsInventory: String)-> AnyPublisher<ProductPage, Never> {
        
        guard let url = URL(string: host + "/product/" + IDRGGoodsInventory)
               else {
            return Just(ProductPage()).eraseToAnyPublisher()
           }
        
        let request = makeRequest(url: url, httpMethod: "GET")
        
        do
        {
            return execute(forRequest: request)
        }
        catch
        {
            return Just(ProductPage()).eraseToAnyPublisher()
        }
    }
    
    

    func fetchByCategory(idRFCategory: String, accessToken: String)-> AnyPublisher<ProductPage, Never> {

           guard let url = URL(string: IPBSettings.shared.hostIEC + "/iecommercecore/api/v1/productset/bycategory/" + idRFCategory + "/"+accessToken+"/1/120/0/0")
               else {
            return Just(ProductPage()).eraseToAnyPublisher()
           }


        Test(url: url)


        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(formatter)

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")

            return URLSession.shared.dataTaskPublisher(for: request)
                .map{$0.data}
                .decode(type: ResultProducts.self, decoder: decoder)
                .map{$0.data!}
                .replaceError(with: ProductPage())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
        }
    
    
    func makeRequest(url: URL, httpMethod: String)->URLRequest
    {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(IPBSettings.shared.accessKey, forHTTPHeaderField: "AccessKey")
        
        return request
    }
    
    func execute(forRequest: URLRequest)->AnyPublisher<ProductPage, Never>
    {
        return URLSession.shared.dataTaskPublisher(for: forRequest)
            .map{$0.data}
            .decode(type: ResultProducts.self, decoder: getDefaultDecoder())
            .map{$0.data!}
            .replaceError(with: ProductPage())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
        
    
    
    
    ///-------------------Tests-------------
    
    
    

    func Test(url: URL?)
    {
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
       
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        do
        {


            request.httpBody = nil

            let task = URLSession.shared.dataTask(with: request) {(data, response, error) in

                if let error = error {
                    print("Error took place \(error)")
                    return
                }
                guard let data = data else {return}

                if let jsonString = String(data: data, encoding: .utf8) {
                    print(jsonString)
                }

                do{
                    let decoder = JSONDecoder()

                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"

                    decoder.dateDecodingStrategy = .formatted(formatter)

                    let todoItemModel = try decoder.decode(ResultProducts.self, from: data)
                    print("Response data:\n \(todoItemModel)")
                    print("todoItemModel result: \(todoItemModel.result!.status)")
                }catch let jsonErr{
                    print(jsonErr)
               }


        }
        task.resume()
        }

    }

}



