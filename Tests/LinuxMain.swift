import XCTest

import ipbswiftapiTests

var tests = [XCTestCaseEntry]()
tests += ipbswiftapiTests.allTests()
XCTMain(tests)
